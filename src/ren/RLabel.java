package ren.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.SwingConstants;

import javax.swing.JLabel;

public class RLabel extends JLabel
{
  private String texto;
  public RLabel(String txt, Color color){
    super(txt, SwingConstants.CENTER);
    this.setFont(new Font("Serif", Font.BOLD, 22));
    this.setForeground(color);
    this.setBackground(color);
    this.setOpaque(true);
    this.texto = this.getText();
  }

  public void pintar(Color color){
    this.setText(this.texto);
    this.setBackground(color);
    this.setForeground(color);
  }

  public void display(int name){
    int size = this.texto.length();
    String txt = "" + name;
    String answer = "";
    int missing = size - txt.length();
    boolean fix = missing % 2 == 0;
    for(int i = 0; i < ( fix ? missing/2 : (missing - 1)/2 ) ; i++){
      answer += " ";
    }
    answer += txt;
    int upper = size - answer.length() + ( fix ? 1 : 0 );
    for(int j = 0; j < upper; j++){
      answer += " ";
    }
    this.setText(answer);
    this.setForeground(Color.black);
  }

  public void resize(int size){
        this.setFont(new Font("Serif", Font.BOLD, size));
  }
}
