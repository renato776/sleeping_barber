package ren.main;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.GroupLayout;

import java.util.Random;

public class Main extends javax.swing.JFrame
{
  private JPanel body;
  RLabel [] lbl_asientos;
  JLabel [] wall;
  RLabel lbl_barbero;

  public static Asiento [] asientos;
  public static Barbero barber;

  public Main(){
    super("El barbero dormilon");

    this.setMinimumSize(new Dimension(800,400));
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    init_components();

    barber = new Barbero(lbl_barbero);
    
    this.pack();
  }

  private GroupLayout.SequentialGroup get_parallel_labels(GroupLayout layout){
    GroupLayout.ParallelGroup [] gp = new GroupLayout.ParallelGroup[3];

    for(int i = 0; i<3; i++)
      gp[i] = layout.createParallelGroup(GroupLayout.Alignment.LEADING);

    for(int i = 0; i<20; i++){
        gp[2].addComponent(lbl_asientos[i]);
    }

    for(int i = 0; i<this.wall.length; i++){
        gp[1].addComponent(this.wall[i]);
    }

    gp[0].addComponent(lbl_barbero);


    GroupLayout.SequentialGroup sg = layout.createSequentialGroup();
    for(int i = 0; i<3; i++)
      sg.addGroup(gp[i]);

    return sg;
  }

  private GroupLayout.ParallelGroup get_sequential_labels(GroupLayout layout){
    GroupLayout.SequentialGroup sg[] = new GroupLayout.SequentialGroup[3];

    for(int i = 0; i<3; i++)
      sg[i] = layout.createSequentialGroup();

    for(int i = 0; i<20; i++)
      sg[2].addComponent(lbl_asientos[i]);

    for(int i = 0; i<this.wall.length; i++)
      sg[1].addComponent(this.wall[i]);

    sg[0].addComponent(lbl_barbero);

    GroupLayout.ParallelGroup gp = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
    for(int i = 0; i<3; i++)
      gp.addGroup(sg[i]);

    return gp;
  }

  private void build_wall(){
    JLabel [] pared = new JLabel[30];
    for( int i = 0; i<30; i++){
        RLabel aux = new RLabel("R",Color.red);
        aux.resize(12);
        pared[i] = aux;
    }
    this.wall = pared;
  }

  private void init_components(){

    this.body = new JPanel();
    GroupLayout layout = new GroupLayout(this.body);
    this.body.setLayout(layout);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);

    lbl_barbero = new RLabel(seat_str, Barbero.durmiendo);

    this.lbl_asientos = new RLabel[20];
    asientos = new Asiento[20];
    for( int i = 0; i<20; i++){
        this.lbl_asientos[i] =  new RLabel(seat_str, Asiento.desocupado);
        asientos[i] = new Asiento(lbl_asientos[i]);
    }

    this.build_wall();

    layout.setHorizontalGroup(
              layout.createSequentialGroup()
                .addGroup( this.get_parallel_labels(layout) ));

    layout.setVerticalGroup( this.get_sequential_labels(layout) );

    this.getContentPane().add(this.body);
  }

  public static void main(String[] args){

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true); 
                new Solution(asientos, barber).start();
            }
        });
  }

  static String seat_str = "";

  static {
    for( int i = 0; i < 13; i++) seat_str += " ";
  }
}
