package ren.errors;

public class GenericError extends RuntimeException
{
    public GenericError(final String message) {
        super( "An error has occurred:  " + message );
    }
}
