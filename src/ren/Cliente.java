package ren.main;

import java.awt.Color;
import java.util.Random;

public class Cliente extends Thread
{
  public int name;
  private boolean pelo_largo;
  public Asiento[] asientos;
  public Barbero barber;
  private int lugar;

  public Cliente(Asiento[] asientos, Barbero barber, int name){
    this.name = name;
    this.pelo_largo = true;
    this.asientos = asientos;
    this.barber = barber;
    this.lugar = -1;
  }

  @Override
  public void run() {
      while(pelo_largo){
          if(barber.ocupado()){
            if(this.lugar >= 0) continue; //Seguimos esperando ....
            for(int i = 0; i<asientos.length; i++){
              if(asientos[i].disponible()){
                asientos[i].ocupar(this.name); //Ocupamos el asiento.
                this.lugar = i; //Recordamos el lugar.
                break; //Ya apartamos el lugar.
              }
            }
            if( this.lugar < 0 ){
              
              break; //No hay asientos disponibles, nos vamos;
            }
            else continue; //Empezamos la espera.
          }else{
            /* The Starvation problem
            if(!barber.apartar()) continue; // Works, but creates an Starvation problem, since 
                                            //new Threads can aquire the lock regardless
                                            // of previous long waiting threads.
            */
            // True solution:
            boolean toca = true;
            for(int i = 0; i<asientos.length; i++){
              if(!asientos[i].disponible()){ //Alguien ya esta esperando, debemos revisar si llego antes.
                if(asientos[i].owner < this.name) toca = false;
              }
            }
            if(!toca) continue; //Hay alguien con mayor prioridad, aun no nos toca.
            if(!barber.apartar()) continue; //Alguien fue mas rapido, seguimos esperando ...
            
            if(lugar >= 0){ //Ya estabamos esperando.
              asientos[lugar].desocupar(); //Desocupamos el asiento.
              this.lugar = -1; //Dejamos de esperar.
            }
            barber.cortar(this.name); //Despertamos al barbero y nos cortan el pelo;
            pelo_largo = false; //Confirmamos;
            barber.dormir(); //El barbero duerme de nuevo.
            
            break; //Salimos;
          }
      }
  }
    
}
