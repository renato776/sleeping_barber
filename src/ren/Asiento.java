package ren.main;

import java.awt.Color;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class Asiento {
  public static Color desocupado = Color.DARK_GRAY;
  public static Color ocupado = Color.CYAN;

  private RLabel lbl;
  public int owner;
  protected ReentrantLock candado;

  public Asiento(RLabel lbl){
    this.owner = -1;
    this.candado = new ReentrantLock();
    this.lbl = lbl;
    this.lbl.pintar(desocupado);
  }

  public void desocupar(){
    this.owner = -1;
    this.candado.unlock();
    this.lbl.pintar(desocupado);
  }

  public boolean disponible(){
        return !this.candado.isLocked();
  }

  public void ocupar(int name){
    if(!this.candado.tryLock()) return; //Si el lock falla, salimos.
    this.owner = name;
    this.lbl.pintar(ocupado);
    this.lbl.display(name);
  }

}
