package ren.main;

import java.util.Random;

public class Solution extends Thread
{
  public Asiento[] asientos;
  public Barbero barber;

  public Solution(Asiento[] asientos, Barbero barber){
    this.asientos = asientos;
    this.barber = barber;
  }

  @Override
  public void run() {
    int name = 0;
    while(true){
      Cliente c = new Cliente(asientos, barber, name); //Creamos un nuevo cliente.
      name++;
      Thread t = new Thread(c); //Aseguramos la creacion de un nuevo Hilo
      t.start(); //Soltamos al nuevo cliente, cuando el cliente finaliza, el solo se destruye.
      try{
        Random rand = new Random();
        int random_time = rand.nextInt(50) + 50;
        Thread.sleep(random_time*40); //Esperamos antes de generar el nuevo cliente.
      } catch(Exception ex){ }
    }
  }
}
