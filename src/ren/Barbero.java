package ren.main;

import java.awt.Color;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class Barbero {
  public static Color durmiendo = Color.DARK_GRAY;
  public static Color ocupado = Color.MAGENTA;

  private RLabel lbl;
  protected ReentrantLock candado;

  public Barbero(RLabel lbl){
    this.lbl = lbl;
    this.candado = new ReentrantLock();
  }

  public boolean ocupado(){
        return this.candado.isLocked();
  }

  public boolean apartar(){
    return this.candado.tryLock(); //Despertamos al barbero y lo apartamos.
  }
  public void cortar(int name){
    this.lbl.pintar(ocupado);
    this.lbl.display(name);
    try {
      Random rand = new Random();
      int random_time = (rand.nextInt(7) + 3)*1000;
      Thread.sleep(random_time); //Cortando pelo
    } 
    catch (Exception e) {
      if(this.candado.isLocked()) this.candado.unlock(); //Por si acaso algo falla, debemos revisar
                                                        //que el candado no se haya quedado cerrado.
    }
  }

  public void dormir(){
    this.lbl.pintar(durmiendo);
    this.candado.unlock(); //Volvemos a dormir.
  }

}
